FROM python:3.8.9
ENV PYTHONUNBUFFERED 1

RUN apt-get update \
    && apt-get -y install wget \
    && apt-get -y install redis \
    && apt-get -y install unzip \
    && apt-get -y install libaio-dev

RUN pip install --no-cache-dir --upgrade poetry

WORKDIR /app
COPY . ./
RUN wget -q https://download.oracle.com/otn_software/linux/instantclient/195000/instantclient-basic-linux.x64-19.5.0.0.0dbru.zip
RUN unzip instantclient-basic-linux.x64-19.5.0.0.0dbru.zip
RUN mkdir -p /opt/oracle
RUN mv ./instantclient_19_5 /opt/oracle/instantclient
ENV LD_LIBRARY_PATH /opt/oracle/instantclient

RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "5000"]

