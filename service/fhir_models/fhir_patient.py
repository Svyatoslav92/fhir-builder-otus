from typing import List
from fhir.resources.address import Address
from fhir.resources.coding import Coding
from fhir.resources.coverage import Coverage
from fhir.resources.humanname import HumanName
from fhir.resources.contactpoint import ContactPoint
from fhir.resources.meta import Meta
from fhir.resources.extension import Extension
from fhir.resources.fhirtypes import (
    Date,
    CodeableConceptType,
    Uri,
    FHIRPrimitiveExtensionType,
    PeriodType,
)
from fhir.resources.identifier import Identifier
from fhir.resources.organization import Organization
from fhir.resources.patient import Patient

from service.fhir_models.fhir_models import SnilsFHIR, GuidFHIR, ManagingOrganization, IdentityDocumentFHIR


def date_to_int(date):
    date_int = date.split("-")
    return [int(i) for i in date_int]


class PolicyNumFHIR(Identifier):
    def __init__(self, value):
        super().__init__()
        self.value = value


class PolicySerFHIR(Identifier):
    def __init__(self, value):
        super().__init__()
        self.value = value


class SMOIdentifierFHIR(Identifier):
    def __init__(
            self, smo_rosminzdrav=None, smo_ffoms=None, smo_id=None, ENP=None, smo_terr=None, smo_okato=None, **kw):
        super().__init__(**kw)

        if smo_rosminzdrav:
            self.system = Uri("ru-rosminzdrav:1.2.643.5.1.13.13.99.2.183:SMOCOD".lower())
            self.value = smo_rosminzdrav
        elif smo_ffoms:
            self.system = Uri("ru-ffoms:F002:SMOCOD".lower())
            self.value = smo_ffoms
        elif smo_id:
            self.system = Uri("ru-rosminzrav:1.2.643.5.1.13.13.99.2.183:ID".lower())
            self.value = smo_id
        elif any((ENP, smo_terr, smo_okato)):
            data = []
            if ENP:
                data.append(Coding(system=Uri("ЕНП"), code="ENP", display=ENP))
            if smo_terr:
                data.append(Coding(system=Uri("Территория страхования"), display=smo_terr))
            if smo_okato:
                data.append(Coding(system=Uri("Регион страхования по ОКАТО"), display=smo_okato))
            self.type = CodeableConceptType(coding=data)


class SMOFHIR(Organization):
    def __init__(self, smo_name=None, smo_code=None, smo_id=None, ENP=None, smo_terr=None, smo_okato=None, **data):
        super().__init__(**data)
        self.name = smo_name
        self.identifier = [
            SMOIdentifierFHIR(ENP=ENP, smo_terr=smo_terr, smo_okato=smo_okato),
            # Значение из
            # https://nsi.rosminzdrav.ru/#!/refbook/1.2.643.5.1.13.13.99.2.183/
            # по полю SMOCOD
            SMOIdentifierFHIR(smo_rosminzdrav=smo_code),
            # Значение из
            # http://nsi.ffoms.ru/#refbookList?refbookList:$active=1&refbookList:$selectedId=7409/refbookList.refbookList.view$119v7409?
            # по полю SMOCOD
            SMOIdentifierFHIR(smo_ffoms=smo_code),
            # Значение из
            # https://nsi.rosminzdrav.ru/#!/refbook/1.2.643.5.1.13.13.99.2.183/
            # по полю ID
            SMOIdentifierFHIR(smo_id=smo_id),
        ]


class CoverageFHIR(Coverage):
    def __init__(
        self,
        policy_type_code=None,
        policy_type_id=None,
        policy_type_name=None,
        policy_series=None,
        policy_number=None,
        policy_begin=None,
        policy_end=None,
        spr_versions=None,
        **data
    ):
        super().__init__(**data)
        self.type = CodeableConceptType(
            coding=[
                Coding(
                    system=Uri("hostco-iemk:hst0065:codepolicytype".lower()),
                    code=policy_type_code,
                    version=spr_versions.get("HST0065"),
                    display=policy_type_name,
                ),
                Coding(
                    system=Uri("komtek-rrp:hst0065:idpolicytype".lower()),
                    code=policy_type_id,
                    version=spr_versions.get("HST0065"),
                    display=policy_type_name,
                ),
            ],
            text=policy_type_name,
        )

        self.identifier = [
            PolicyNumFHIR(value=policy_number),
            PolicySerFHIR(value=policy_series),
        ]
        self.period = PeriodType(start=policy_begin, end=policy_end)


class AddressFias(Address):
    def __init__(
        self,
        city=None,  # CityCode
        district=None,  # LocalityCode & SettlementCode
        state=None,  # RegionCode
        street=None,  # StreetCode
        house=None,  # HouseCode
        flat=None,  # Flat
        text=None,  # FullAddress
        use="billing",  # 'home' - адрес фактический;  'billing' = адрес регистрации.
    ):
        super().__init__()
        self.city = city
        self.district = district
        self.line = self._get_line([street, house])
        if flat:
            self.line.append(flat)
        self.state = state
        self.text = text
        self.use = use

    @staticmethod
    def _get_line(items: list) -> List[str]:
        line = []
        for item in items:
            if item:
                line.append(item)
            else:
                line.append(" ")
        return line


class HumanNameFHIR(HumanName):
    def __init__(self, family=None, name=None, father_name=None, use=None):
        super().__init__()
        self.family = family
        self.use = use
        if father_name:
            self.given = [name, father_name]
        else:
            self.given = [name]


class ContactPointFHIR(ContactPoint):
    def __init__(self, system=None, value=None, use=None):
        super().__init__()
        self.system = system
        self.value = value
        self.use = use


class PatientFHIR(Patient):
    def __init__(
        self,
        id=None,
        snils=None,
        guid=None,
        mcod=None,
        fed_oid=None,
        family=None,
        old_family=None,
        name=None,
        father_name=None,
        sex=None,
        inv_group=None,
        shift_worker=None,
        nationality=None,
        marital_status=None,
        education=None,
        citizenship=None,
        identity_docs=None,
        email=None,
        phone=None,
        company_name=None,
        position_name=None,
        birth_date=None,
        birth_place=None,
        address=None,
        contact=None,
        spr_versions=None,
        extension=None,
        **kwargs
    ):
        super().__init__()
        self.id = id
        self.identifier = [
            SnilsFHIR(value=snils),
            GuidFHIR(value=guid),
            ManagingOrganization(value=mcod),
            ManagingOrganization(value=fed_oid),
        ]
        self.name = [HumanNameFHIR(family, name, father_name, "official")]
        if old_family not in (None, ""):
            self.name.append(HumanNameFHIR(old_family, name, father_name, "old"))
        self.birthDate = Date(*date_to_int(birth_date))
        self.birthDate__ext = FHIRPrimitiveExtensionType(extension=[Extension(valueString=birth_place)])
        self.gender__ext = FHIRPrimitiveExtensionType(
            extension=[
                Extension(valueString=sex),
                Extension(
                    valueCoding=Coding(
                        system=Uri("Классификатор типов пола"),
                        code="C51007",
                        version=spr_versions.get("C51007"),
                    ),
                ),
            ]
        )
        self.multipleBirthBoolean__ext = FHIRPrimitiveExtensionType(
            extension=[
                Extension(
                    valueCodeableConcept=CodeableConceptType(
                        coding=[
                            Coding(
                                system=Uri("Справочник инвалидности"),
                                code="HST0010",
                                version=spr_versions.get("HST0010"),
                                display=inv_group,
                            ),
                            Coding(
                                system=Uri("Классификатор национальностей"),
                                code="STR470",
                                version=spr_versions.get("STR470"),
                                display=nationality,
                            ),
                            Coding(
                                system=Uri("Классификатор образования"),
                                code="STR337",
                                version=spr_versions.get("STR337"),
                                display=education,
                            ),
                            Coding(
                                system=Uri("Общероссийский классификатор стран мира"),
                                code="O00015",
                                version=spr_versions.get("O00015"),
                                display=citizenship,
                            ),
                        ]
                    )
                ),
                Extension(valueString=shift_worker),
                Extension(valueString=company_name),
                Extension(valueString=position_name),
            ]
        )
        self.maritalStatus = CodeableConceptType(
            coding=[
                Coding(
                    system=Uri("Справочник семейное положение"),
                    code="HST0080",
                    version=spr_versions.get("HST0080"),
                    display=marital_status,
                ),
            ]
        )
        self.telecom = [
            ContactPointFHIR("email", email),
            ContactPointFHIR("phone", phone),
        ]
        self.address = address
        self.contact = contact
        self.add_identifier(identity_docs, spr_versions)
        self.extension = extension if len(extension) > 0 else None

    def add_identifier(self, identity_docs: list, directory_version: dict):
        if identity_docs:
            for doc in identity_docs:
                self.identifier.append(IdentityDocumentFHIR(**doc, directory_version=directory_version))


class PatientFHIRPIDDATEREZ(Patient):
    def __init__(
        self,
        id=None,
        lastUpdated=None,
    ):
        super().__init__()
        self.id = id
        self.meta = Meta(lastUpdated=lastUpdated)
