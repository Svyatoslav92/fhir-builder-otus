from fhir.resources.coding import Coding
from fhir.resources.fhirtypes import CodeableConceptType, Uri, PeriodType, ReferenceType
from fhir.resources.identifier import Identifier


def date_to_int(date):
    date_int = date.split("-")
    return [int(i) for i in date_int]


class GuidFHIR(Identifier):
    def __init__(self, value):
        super().__init__()
        self.value = value


class SnilsFHIR(Identifier):
    def __init__(self, value):
        super().__init__()
        self.type = CodeableConceptType(
            coding=[
                Coding(
                    system=Uri("http://terminology.hl7.org/CodeSystem/v2-0203"),
                    code="SB",
                    display="Social Beneficiary Identifier",
                ),
                Coding(
                    system=Uri("urn:CodeSystem:ru-identifier-type"),
                    code="SNILS",
                    display="СНИЛС",
                ),
            ]
        )
        self.system = Uri("urn:identity:snils")
        self.value = value


class InnFHIR(Identifier):
    def __init__(self, value):
        super().__init__()
        self.type = CodeableConceptType(
            coding=[
                Coding(
                    system=Uri("http://terminology.hl7.org/CodeSystem/v2-0203"),
                    code="TAX",
                    display="Tax ID number",
                ),
                Coding(
                    system=Uri("urn:CodeSystem:ru-identifier-type"),
                    code="INN",
                    display="ИНН",
                ),
            ]
        )
        self.system = Uri("urn:identity:inn")
        self.value = value


class IdentityDocumentFHIR(Identifier):
    def __init__(
        self,
        series=None,
        number=None,
        start=None,
        end=None,
        issuer=None,
        doc_type_code=None,
        directory_version=None,
        doc_type_name=None,
    ):
        super().__init__()
        self._set_value(series, number)
        self.period = PeriodType(
            start=start,
            end=end,
        )
        self.assigner = ReferenceType(display=issuer)
        self._set_type(doc_type_code, doc_type_name, directory_version)

    def _set_value(self, series, number):
        if series or number:
            if series and number:
                self.value = series + "_" + number
            if series is None:
                self.value = number

    def _set_type(self, doc_type_code: int, doc_type_name: str, spr: dict):
        coding = [
            Coding(
                system=Uri("ru-rosminzdrav:1.2.643.5.1.13.13.99.2.48:id"),
                code=str(doc_type_code),
                version=spr.get("1.2.643.5.1.13.13.99.2.48"),
            )
        ]
        if doc_type_code == 1:
            coding.append(
                Coding(
                    system=Uri("http://terminology.hl7.org/CodeSystem/v2-0203"),
                    code="PPN",
                    display="Passport number",
                )
            )
        self.type = CodeableConceptType(coding=coding, text=doc_type_name)


class ManagingOrganization(Identifier):
    def __init__(self, value):
        super().__init__()
        self.type = CodeableConceptType(
            coding=[
                Coding(
                    system="http://nsi.ffoms.ru",
                    code="F003",
                    display="Единый реестр медицинских организаций, "
                    "осуществляющих деятельность в сфере "
                    "обязательного медицинского страхования (MO)",
                )
            ]
        )
        self.value = value
