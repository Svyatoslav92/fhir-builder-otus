import json
from fastapi import status
from abc import abstractmethod
from fastapi.responses import JSONResponse

from service.fhir_models.fhir_operation_outcome import build_operationoutcome


class ServiceError(Exception):
    """Ошибка сервиса."""

    def __init__(self, **kwargs):
        """Инициализация."""
        self.error = build_operationoutcome(**kwargs)
        super().__init__(self.error)

    @abstractmethod
    def response(self) -> JSONResponse:
        """Метод возвращающий ошибку в формате FHIR"""


class FHIRServerError(ServiceError):
    """Ошибка в работе сервиса"""

    def response(self) -> JSONResponse:
        return JSONResponse(
            content=json.loads(self.error.json()),
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


class FHIRNotFoundError(ServiceError):
    """Отсутствуют данные"""

    def response(self) -> JSONResponse:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content=json.loads(self.error.json()),
        )


class FHIRBadRequest(ServiceError):
    """Ошибка запроса"""

    def response(self) -> JSONResponse:
        return JSONResponse(
            content=json.loads(self.error.json()),
            status_code=status.HTTP_400_BAD_REQUEST,
        )
