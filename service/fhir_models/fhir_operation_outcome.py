from typing import Optional

from fhir.resources.operationoutcome import OperationOutcome, OperationOutcomeIssue


def build_operationoutcome(list_issue: Optional[list] = None, **kw) -> OperationOutcome:
    params = {"severity": kw.get("severity", "error")}
    if list_issue is None:
        params.update({'code': kw.get("code", 0),
                       "details": {"text": kw.get("details", "Детали отсутствуют")},
                       "diagnostics": kw.get("diagnostics", None)})

        issue = [OperationOutcomeIssue(**params)]
    else:
        issue = []
        for raw_issue in list_issue:
            issue.append(OperationOutcomeIssue(**raw_issue, **params))

    params.update({"issue": issue})

    return OperationOutcome(issue=issue, id=kw.get("id", None))
