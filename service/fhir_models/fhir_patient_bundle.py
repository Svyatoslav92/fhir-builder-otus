from datetime import datetime as dt
from typing import List
from fhir.resources.bundle import Bundle, BundleEntry
from service.fhir_models.fhir_patient import PatientFHIR, AddressFias, CoverageFHIR, SMOFHIR, HumanNameFHIR,\
    ContactPointFHIR
from fhir.resources.codeableconcept import CodeableConcept
from fhir.resources.coding import Coding
from fhir.resources.extension import Extension
from fhir.resources.reference import Reference

from service.utils import check_age, get_policy_type_id


def build_identity_doc(docs: list) -> List[dict]:
    """
    Формируем список ДУЛ
    """
    new_docs = []

    for doc in docs:
        series = doc.get("SER")
        number = doc.get("NUM")
        date_v = doc.get("DATE_V")
        if date_v:
            start = dt.strftime(date_v, "%Y-%m-%d")
        else:
            start = None

        issuer = doc.get("KEM")
        doc_type_code = doc.get("RID_FED")
        doc_type_name = doc.get("NAME")
        new_docs.append(
            {
                "series": series,
                "number": number,
                "start": start,
                "issuer": issuer,
                "doc_type_name": doc_type_name,
                "doc_type_code": doc_type_code,
            }
        )

    return new_docs


def build_social_status_extension(patient_data):
    """FHIR Extension для Социального статуса
    https://kb.pkzdrav.ru/display/FHIR/patient-socialstatus"""
    social_status_extension = Extension(
        url="https://kb.pkzdrav.ru/display/FHIR/patient-socialstatus",
        valueCodeableConcept=CodeableConcept(
            coding=[
                Coding(
                    system="ru-rosminzdrav:1.2.643.5.1.13.13.11.1038:id",
                    code=patient_data.get("soc_grp").get("social"),
                    version=patient_data.get("spr_versions").get("1.2.643.5.1.13.13.11.1038"),
                ),
                Coding(
                    system="ru-rosminzdrav:1.2.643.5.1.13.13.99.2.374:id",
                    code=patient_data.get("soc_grp").get('fed_id'),
                    version=patient_data.get("spr_versions").get("fed_soc_status"),
                )
            ],
            text=patient_data.get("soc_grp").get('social_name'),
        ),
    )
    return social_status_extension


def build_snils_missing_reason_extension(patient_data):
    """FHIR Extension для причины отсутсвия снилс
    https://kb.pkzdrav.ru/display/FHIR/patient-socialstatus"""
    code = display = None
    if not patient_data.get("patient").get('hst0460') and patient_data.get("patient").get('birth_date'):
        # если у пациента не указана причина отсутствия снилс и его возраст менее 2 месяцев, то передает code = 3
        if check_age(patient_data.get("patient").get('birth_date'), 60):
            code = 3
            display = 'ребенок в возрасте до 2 мес'
            version = "1.0"
    else:
        code = patient_data.get("patient").get('hst0460')
        display = patient_data.get("patient").get("hst0460_name")
        version = patient_data.get("spr_versions").get('hst0460')

    social_status_extension = Extension(
        url="https://kb.pkzdrav.ru/display/FHIR/patient-snilsmissingreason",
        valueCodeableConcept=CodeableConcept(
            coding=[
                Coding(
                    system="komtek-rrp:hst0460:code",
                    code=code,
                    version=version,
                    display=display
                )
            ]
        )
    )
    return social_status_extension


def build_extension(data):
    """Строит  FHIR Extension https://www.hl7.org/fhir/extensibility.html"""
    extension = list()
    if data.get("soc_grp"):
        if data.get("soc_grp").get("social"):
            extension.append(build_social_status_extension(data))
    if data.get("patient").get('hst0460') or\
            (data["patient"]['birth_date'] and check_age(data["patient"]['birth_date'], 60)):
        extension.append(build_snils_missing_reason_extension(data))
    return extension


def build_address(fias_addr):  # noqa: C901
    addresses = []
    if not fias_addr:
        return addresses
    for addr in fias_addr:
        address = AddressFias(**addr)
        addresses.append(address)
    return addresses


def build_contact(contacts):
    _contact = []

    for member in contacts:
        family = member.get("member").get("sname")
        name = member.get("member").get("fname")
        father_name = member.get("member").get("mname")
        _name = HumanNameFHIR(family, name, father_name, "official")
        system = "phone"
        phone = member.get("member").get("phone")
        _telecom = [ContactPointFHIR(system, phone)]
        fiasaddress = member.get("fias_address")
        _addresses = build_address(fiasaddress)
        # если указано 2 типа адреса, то берем второй. если ни одного адреса то None
        _addresses = _addresses[1] if len(_addresses) == 2 else _addresses or None
        _contact.append({"name": _name, "telecom": _telecom, "address": _addresses})

    return _contact


def build_fhir_patient(data):

    if not data['patient'].get('marital_status'):
        msstat = 1 if data['patient'].get('sex') == 1 else 3
        data['patient'].update({'marital_status': msstat})

    extension = build_extension(data)

    if data['patient'].get('birth_date') and not isinstance(data['patient'].get('birth_date'), str):
        birth_date = dt.strftime(data["patient"]['birth_date'], "%Y-%m-%d")
        data['patient'].update({'birth_date': birth_date})

    identity_docs = None
    if data.get('docum'):
        identity_docs = build_identity_doc(data.get('docum'))

    data_patient = {**data.get('patient'),
                    **data.get('hospital', {}),
                    **data.get('work_place', {}),
                    'identity_docs': identity_docs,
                    "extension": extension,
                    "spr_versions": data.get('spr_versions'),
                    "address": build_address(data.get("fias_addr")),
                    'contact': build_contact(data.get('family'))}

    return PatientFHIR(**data_patient)


def build_fhir_policy(data):

    if not data.get('policy'):
        return CoverageFHIR(status="active", payor=[], beneficiary=Reference(), spr_versions=data.get('spr_versions'))

    if not data['policy'].get('policy_series') and not data['policy'].get('enp'):
        data.update({'policy_series': ' '})

    if data['policy'].get('policy_begin') and not isinstance(data['policy'].get('policy_begin'), str):
        data['policy'].update({'policy_begin': dt.strftime(data['policy'].get('policy_begin'), "%Y-%m-%d")})

    if data['policy'].get('policy_end') and not isinstance(data['policy'].get('policy_end'), str):
        data['policy'].update({'policy_end': dt.strftime(data['policy'].get('policy_end'), "%Y-%m-%d")})

    return CoverageFHIR(
        policy_begin=data['policy'].get('policy_begin'),
        policy_end=data['policy'].get('policy_end'),
        policy_number=data['policy'].get('policy_number'),
        policy_series=data['policy'].get('policy_series'),
        policy_type_name=data['policy'].get('policy_type_name'),
        policy_type_id=get_policy_type_id(data['policy'].get('policy_type_code')),
        status="active",
        payor=[],
        beneficiary=Reference(),
        spr_versions=data.get('spr_versions'))


def build_fhir_smo(data):
    if not data.get('policy'):
        return SMOFHIR()

    return SMOFHIR(ENP=data['policy'].get('enp'), **data.get('smk'))


def build_bundle_patient(data):
    return Bundle(
        type="collection",
        entry=[
            BundleEntry(resource=build_fhir_patient(data)),
            BundleEntry(resource=build_fhir_policy(data)),
            BundleEntry(resource=build_fhir_smo(data)),
        ],
    )
