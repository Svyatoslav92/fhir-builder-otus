from datetime import datetime
from typing import Union
from conf.settings import DBS
from service.fhir_models.fhir_exceptions import FHIRBadRequest
from service.redis_store import RedisStore


def mapping_marital_status(marital_status: int):
    values = {1: 1, 2: 2, 3: 1, 4: 2, 5: 5, 6: 5, 7: 4, 8: 4}
    return values.get(marital_status)


def check_snils(snils: str) -> bool:
    if not snils or not isinstance(snils, str):
        return False

    snils = snils.replace("-", "").replace(" ", "")

    if len(snils) != 11:
        return False

    if 1001998 > int(snils[:-2]):
        return False

    def snils_csum(snils):
        k = range(9, 0, -1)
        pairs = zip(k, [int(x) for x in snils.replace("-", "").replace(" ", "")[:-2]])
        return sum([k * v for k, v in pairs])

    csum = snils_csum(snils)

    while csum > 101:
        csum %= 101
    if csum in (100, 101):
        csum = 0

    return csum == int(snils[-2:])


def get_policy_type_id(policy_type_code: int):
    """Функция возвращает необходимый идентификатор полиса согласно его коду.
    Наименование: Справочник типов полисов
    Код в сервисе НСИ: HST0065
    Версия: 1.0 (для заполнения использовалась версия 1.2)

    Параметры
    ---------
    policy_type_code: int
        Тип полиса согласно справочнику

    Возвращает
    ----------
    int:
        Значение идентификатора
    """
    if policy_type_code is None:
        return 5

    values = {
        # Полис ОМС старого образца
        1: 5,
        # Полис ОМС единого образца
        3: 7,
        # Временное свидетельство
        2: 6,
        # Сост на учёте без полиса ОМС
        4: 8,
        # Состояние на учёте без временного свидетельства при приёме заявления в иную организацию
        5: 9,
        # Полис ДМС
        100: 10,
    }

    return values.get(policy_type_code)


def is_from_russia(docs: list) -> bool:
    if docs:
        for doc in docs:
            if doc.get("identity_directory_code") == 1:
                return True
    return False


def check_age(date_b: datetime, days: int) -> bool:
    # функция проверяет больше ли возраст значения days
    if isinstance(date_b, str):
        date_b = datetime.strptime(date_b, "%Y-%m-%d")

    age = datetime.now() - date_b
    return age.days < days


def check_db(db: str) -> bool:
    """Проверка на наличие БД в конфиге"""
    if db not in DBS:
        data = {"code": 1,
                "details": "База данных определена неверно",
                "diagnostics": f"Доступные базы данных: {', '.join(DBS.keys())}"}
        raise FHIRBadRequest(**data)
    return True


def set_cache(key: Union[str, int], val: dict):
    cache = RedisStore()
    cache.cache_set_dict(key, val)
