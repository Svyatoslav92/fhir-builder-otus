import json
from typing import Optional, Union
import redis
import logging
from conf.settings import REDIS_URL, REDIS_SOCKET_TIMEOUT


class RedisStore:

    con = redis.from_url(REDIS_URL, socket_timeout=REDIS_SOCKET_TIMEOUT)

    def __init__(self, cache_time=60):
        self.cache_time = cache_time
        self.attempts = 0

    @classmethod
    def _get_con_redis(cls):
        return cls.con

    @classmethod
    def close(cls):
        cls.con.close()

    def _cache_get(self, key: Union[str, int]):
        redis = self._get_con_redis()
        return redis.get(key)

    def _cache_set(self, key: Union[str, int], value, cache_time=None) -> bool:
        cache_time = cache_time if cache_time else self.cache_time
        redis = self._get_con_redis()
        redis.setex(key, time=cache_time, value=value)
        return True

    def cache_get_dict(self, key: Union[str, int]) -> Union[dict, None]:
        try:
            json_obj = self._cache_get(key)
        except redis.exceptions.ConnectionError as err:
            logging.warning(f"Соединение с Redis отсутствует: {err}")
            return None
        return json.loads(json_obj) if json_obj else None

    def cache_set_dict(self, key: Union[str, int], value: dict, cache_time: Optional[int] = None) -> bool:
        cache_time = cache_time if cache_time else self.cache_time
        json_obj = json.dumps(value, default=str)
        try:
            self._cache_set(key, json_obj, cache_time)
            return True
        except redis.exceptions.ConnectionError as err:
            logging.warning(f"Соединение с Redis отсутствует: {err}")
            return False
