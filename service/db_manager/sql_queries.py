# Вернуть медицинскую организацию
GetHospital = "select lpu_id, fed_oid from lpu.tune where lid = :lid"

# Вернуть пациента
GetPassportByPid = """
select  l_p.sname
       ,l_p.sname_old
       ,l_p.fname
       ,l_p.mname
       ,decode(l_p.pol, 'М', 1, 'Ж', 2, 3) sex
       ,l_p.phone
       ,decode(l_p.vahta, 1, 1, 0) shift_worker
       ,l_p.snils
       ,l_p.country_id
       ,l_p.date_b
       ,l_p.birthplace
       ,l_p.mstatus
       ,l_p.education
       ,l_p.email
       ,r.hst0460
       ,r.s_name as hst0460_name
       ,l_p.inv
       ,coalesce(a_g_n.tsod_nationality, 1) nationality
       ,l_p.pid
from lpu.pasport l_p
left join adm_glb.nacional a_g_n
on l_p.nacional = a_g_n.rid
left join adm_glb.no_snils_reason r
on l_p.no_snils_reason = r.rid
where l_p.pid = :pid
"""

# Вернуть полис ОМС(ПК Здрав)
GetPolicyByPid = """
select
    vid.name,
    p.policynum,
    p.policyser,
    p.enp,
    p.date_s,
    p.date_e,
    p.lid,
    coalesce(vid.rid_hmao, 1) rid_hmao
from lpu.policy p
inner join adm_glb.vid_p vid
on
    p.vid_p = vid.rid
where
    p.pid = :pid"""

# Вернуть документы
GetPDocumByPid = """
select p.ser, p.num, p.date_v, p.kem, v.name, v.rid_fed
from lpu.p_docum p join adm_glb.vdk v on p.vdk = v.rid where p.pid = :pid"""

# Вернуть данные родственников (идентификатор, фамилия, имя, отчетство, телефон)
GetFamilyByPid = """
select
    l_p.pid, l_p.sname, l_p.fname, l_p.mname, l_p.phone
from
    lpu.pasport l_p
inner join lpu.family_info l_f_i
on
    l_p.pid = l_f_i.rel_pid
where
    l_f_i.pid = :pid"""


# Вернуть социальный статус
GetSocGrpByPid = """
select
    coalesce(sg.fed_id_1038, 10) as social,
    sg.fed_id,
    (case
        when(sg.fed_id_1038 is null) then 'ПРОЧИЕ'
        else sg.name
     end
    ) as social_name
from
    lpu.PASPORT_SOCGRP ps
    left join adm_glb.SOCGRP sg on ps.social = sg.rid
where
      ps.pid = :pid"""

# Вернуть страховую компанию
GetSMKByLid = """
select
    sm.s_name,
    sm.smocod,
    sm.id_rosminzdrav,
    sub_rf.subject as smo_terr,
    sm.okato
from adm_glb.smk sm
    join nsi.fed_subjects_rf sub_rf on sm.foms = sub_rf.id
where sm.smk = :lid
"""

# место работы
GetWorkPlaceByPid = "select workplace, workpos from lpu.viemk_patient where pid = :pid"

# Вернуть врача
GetDoctor = """
select
    u.SNILS,
    u.SNAME,
    u.FNAME,
    u.MNAME,
    u.DATE_B,
    u.BIRTHPLACE,
    u.SEX,
    u.EMAIL,
    u.PHONE,
    u.INN,
    ps.SER,
    ps.NUM,
    ps.DATE_V,
    ps.KEM,
    ps.VDK,
    ms.fed_00365_tsod as position,
    coalesce(pl.frmo_subdiv_oid, sb.frmo_oid) as subdiv_code,
    pl.frmo_oid as depart_code
from lpu.users u
    left join lpu.doctor d on u.id_user = d.id_user
    left join lpu.USER_DOCUM ps on u.id_user = ps.ID_USER
    inner join adm_glb.medspec ms on d.speccode = ms.rid
    inner join lpu.place pl on d.place = pl.place
    inner join lpu.s_lpu sb on sb.s_lpu = pl.s_lpu
where
    u.id_user = :pid"""

# адресс фиас
GetFiasAddress = """
select
       decode(a.object_type, 1, 'home', 2, 'billing', 0) address_type
       ,a.region_code
       ,decode(aolevel, 4, a.city_code, null) as city_code
       ,decode(aolevel, 4, null, a.city_code) as locality_code
       ,a.street_code
       ,a.house_code
       ,a.flat
       ,a.region_name
         || ', ' ||
        a.city_name
         || ', ' ||
        a.street_name ||
        nvl2(a.house_num , ', д.' || a.house_num , '') ||
        nvl2(a.build_num, ', корп.' || a.build_num, '') ||
        nvl2(a.struc_num, ', стр.' || a.struc_num, '') ||
        nvl2(a.flat, ', кв.' || a.flat, '') as full_address
from lpu.addresses a
     join dual  on a.object_id = :pid
     join nsi.fias_addrob fc on fc.aoguid = a.city_code
where fc.actstatus = 1
  and fc.livestatus = 1
  and fc.nextid is null
  and a.object_type in (1, 2)
"""

# получение версий справочников
GetDirectoryVersion = """
select distinct s_code, s_version from adm_glb.tsod_spr
where upper(s_code) in (
    'FRMR_POST', 'FRMO_DEPART', 'FRMO_SUBDIV',
    'FRMR_SPEC', 'HST0080', 'STR470',
    'HST0010', 'STR337',
    'O00015', 'C51009', 'C51007',
    'HST0065', 'HST0460')
    or oid in ('1.2.643.5.1.13.13.99.2.48', '1.2.643.5.1.13.13.99.2.374', '1.2.643.5.1.13.13.11.1038')"""


#  Вернуть PID пациента по TSODE_GUID
GetPidByTsodeGuid = """
select id.rid_instance
from lpu.iemk_documents id
where
    id.log_object_type = 1
    and upper(id.tsode_guid) = upper(:tsode_guid)"""
