import logging
import cx_Oracle
import cx_Oracle_async

from conf.settings import DBS, EXECUTE_TIMEOUT_SQL
from service.db_manager.exceptions import ExecSQLTimeoutError


class DBConnector:

    def __init__(self, db_name):
        self.db_name = db_name
        self.con = None

    # creats new connection
    async def create_connection(self):
        return await cx_Oracle_async.create_pool(**DBS[self.db_name])


class DBConnection:
    """
    Класс хранит экземпляры объектов подключения к БД
    Методы выполняют переданный sql с параметрами и без
    """
    connections = {}

    @classmethod
    async def get_connection(cls, db_name, new=False):
        """Creates return new Singleton database connection"""
        if new or db_name not in cls.connections:
            logging.info(f"Новое подключение к {db_name}")
            cls.connections[db_name] = await DBConnector(db_name).create_connection()
        else:
            logging.info(f'Подключение к {db_name}')

        cls.connections[db_name].callTimeout = EXECUTE_TIMEOUT_SQL
        return cls.connections[db_name]

    @classmethod
    async def close_connection(cls, db_name):
        # Закрыть подключение к БД
        if cls.connections:
            con = cls.connections.get(db_name)
            await con.close() if con is not None else None

    @classmethod
    async def close_connections(cls):
        # Закрыть все подключения к БД
        if cls.connections:
            for db_name in cls.connections.keys():
                await cls.close_connection(db_name)
                logging.info(f"Connection {db_name} close")

    @classmethod
    async def execute_query(cls, sql: str, db_name: str, params: dict = None, many: bool = False) -> list or dict:
        """
        execute query on singleton db connection
        :param sql: запрос SQL
        :type sql: str

        :param params: параметры запроса SQL, при значении None - для запросов без параметров
        :type params: dict

        :param many: признак, когда в результате запроса ожидается набор записей (True) или единственная запись (False)
        :type many: bool

        :returns: список записей при many=True, единственная запись при many=False

        """
        conn_pool = await cls.get_connection(db_name)
        conn = await conn_pool.acquire()
        try:
            cursor = await conn.cursor()
        except cx_Oracle.InterfaceError as e:
            print(f"пере подключение к БД {db_name}: {e}")
            connection = await cls.get_connection(db_name, new=True)  # Create new connection
            cursor = await connection.cursor()
        except cx_Oracle.DatabaseError as e:
            print(f"ошибка  БД {db_name}: {e}")
            connection = await cls.get_connection(db_name, new=True)  # Create new connection
            cursor = connection.cursor()

        try:
            if params:
                await cursor.execute(sql, params)
            else:
                await cursor.execute(sql)
        except cx_Oracle.DatabaseError as e:
            await cursor.close()
            await cls.close_connection(db_name)
            # проверяем исключение на тип ошибки истечения таймаута запроса sql
            if 'DPI-1067: call timeout of' in str(e):
                raise ExecSQLTimeoutError('')
            raise cx_Oracle.DatabaseError from e  # сохраняем информацию о "родительском" исключении

        if many:
            result = await cursor.fetchall()
            return result

        return await cursor.fetchone()

    @classmethod
    def perform_data_entry(cls, sql: str, params: dict, db_name: str):
        try:
            cls._execute_sql(sql, params, db_name)
        except cx_Oracle.InterfaceError as e:
            logging.info(f"ошибка InterfaceError {e}")
            # via new connection
            cls._execute_sql(sql, params, db_name, new_conn=True)
        except cx_Oracle.DatabaseError as e:
            logging.info(f"ошибка DatabaseError {e}")
            cls.close_connection(db_name)
            if 'DPI-1067: call timeout of' in str(e):
                raise ExecSQLTimeoutError(str(e))
            elif 'DPI-1010: not connected' in str(e) or 'DPI-1080: connection was closed' in str(e):
                # via new connection
                cls._execute_sql(sql, params, db_name, new_conn=True)
            else:
                raise
        except Exception as e:
            logging.info(f"Неизвестная ошибка {e}")
            raise
