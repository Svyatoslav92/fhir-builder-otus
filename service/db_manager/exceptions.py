
class ExecSQLTimeoutError(Exception):
    """Ошибка по истечению таймаута на выполнение sql."""

    def __init__(self, msg):
        self.msg = str(msg)

    def __str__(self):
        return self.msg
