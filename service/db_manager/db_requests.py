from service.db_manager.connector import DBConnection
from service.db_manager.sql_queries import GetHospital, GetPolicyByPid, GetPDocumByPid, GetSocGrpByPid, \
    GetPassportByPid, GetSMKByLid, GetWorkPlaceByPid, GetFiasAddress, GetDirectoryVersion, GetFamilyByPid
from service.fhir_models.fhir_exceptions import FHIRNotFoundError
from service.redis_store import RedisStore
from service.utils import check_db


async def get_patient(pid, db_name):
    columns = ('family', 'old_family', 'name', 'father_name', 'sex', 'phone', 'shift_worker', 'snils', 'citizenship',
               'birth_date', 'birth_place', 'marital_status', 'education', 'email', 'hst0460', 'hst0460_name',
               'inv_group', 'nationality', 'id',)
    result = await DBConnection.execute_query(GetPassportByPid, params={'pid': pid}, db_name=db_name)
    return dict(zip(columns, result)) if result else None


async def get_hospital(lid, db_name):
    result = await DBConnection.execute_query(GetHospital, params={'lid': lid}, db_name=db_name)
    return {'mcod': result[0], 'fed_oid': result[1]} if result else None


async def get_policy(pid, db_name):
    columns = (
        'policy_type_name', 'policy_number', 'policy_series', 'enp', 'policy_begin', 'policy_end', 'lid', 'rid_hmao')
    result = await DBConnection.execute_query(GetPolicyByPid, params={'pid': pid}, db_name=db_name)
    return dict(zip(columns, result)) if result else None


async def get_docum(pid, db_name):
    columns = ('ser', 'num', 'date_v', 'kem', 'name', 'rid_fed',)
    result = await DBConnection.execute_query(GetPDocumByPid, params={'pid': pid}, db_name=db_name, many=True)
    return [dict(zip(columns, row)) for row in result] if result else None


async def get_socgrp(pid, db_name):
    columns = ('social', 'fed_id', 'name', 'social_name')
    result = await DBConnection.execute_query(GetSocGrpByPid, params={'pid': pid}, db_name=db_name)
    return dict(zip(columns, result)) if result else None


async def get_smk(lid, db_name):
    columns = ('smo_name', 'smo_code', 'smo_id', 'smo_terr', 'smo_okato',)
    result = await DBConnection.execute_query(GetSMKByLid, params={'lid': lid}, db_name=db_name)
    return dict(zip(columns, result)) if result else None


async def get_workplace(pid, db_name):
    columns = ('company_name', 'position_name',)
    result = await DBConnection.execute_query(GetWorkPlaceByPid, params={'pid': pid}, db_name=db_name)
    return dict(zip(columns, result)) if result else None


async def get_fias_addr(pid, db_name):
    columns = ('use', 'state', 'city', 'district', 'street', 'house', 'flat', 'text',)
    result = await DBConnection.execute_query(GetFiasAddress, params={'pid': pid}, db_name=db_name, many=True)
    return [dict(zip(columns, row)) for row in result] if result else None


async def get_spr_versions(db_name):
    result = await DBConnection.execute_query(GetDirectoryVersion, db_name=db_name, many=True)
    return {row[0]: row[1] for row in result}


async def get_family(pid, db_name):
    columns = ('pid', 'sname', 'fname', 'mname', 'phone')
    result = await DBConnection.execute_query(GetFamilyByPid, params={'pid': pid}, db_name=db_name, many=True)
    return [dict(zip(columns, row)) for row in result] if result else []


async def get_family_data(pid: int, db_name: str) -> list:
    """Функция ищет семью по заданному pid.

    Параметры
    ---------
    pid: int Идентификатор пользователя в БД
    db_name: str аббревиатура БД МО

    Возвращает
    ----------
    dict:
        Словарь с данными из БД или пустой словарь
    """

    family_info = []
    family_data = await get_family(pid, db_name)

    if family_data:
        for member in family_data:
            fiasaddress = await get_fias_addr(member.get("pid"), db_name)
            _data = {"member": member, "fias_address": fiasaddress}
            family_info.append(_data)

    return family_info


async def get_data_from_db_patient(pid: int, lid: int, db_name: str, required_data: list = None) -> dict: # NOQA
    if required_data is None:
        required_data = ["Hospital", "Policy", "Docum", "Socgrp", "Smk", "Workplace", "Fiasaddress"]
    result = {}

    check_db(db_name)

    patient_data = await get_patient(pid, db_name)

    if not patient_data:
        raise FHIRNotFoundError(id=pid, code=2, details=f"Пациент {pid} не найден в БД {db_name}")

    result.update({"patient": patient_data})

    cache = RedisStore()

    if "Hospital" in required_data:
        # Медицинская организация
        cache_hosp = cache.cache_get_dict(key=lid)
        hosp_data = cache_hosp or await get_hospital(lid, db_name)
        if hosp_data:
            result.update({"hospital": hosp_data})
            if not cache_hosp:
                cache.cache_set_dict(key=lid, value=hosp_data, cache_time=3600)

    if "Policy" in required_data:
        # Полис ОМС (ПК Здрав)
        result.update({"policy": await get_policy(pid, db_name)})

        smk_data = None
        if result.get("policy"):
            # Страховая компания
            lid_smk = result["policy"].get("lid")
            smk_data = await get_smk(lid_smk, db_name)
            result.update({"smk": smk_data})

    if "Docum" in required_data:
        # Документы (ПК Здрав)
        docum_data = await get_docum(pid, db_name)
        if docum_data:
            result.update({"docum": docum_data})

    if "Socgrp" in required_data:
        # Социальный статус
        socgrp_data = await get_socgrp(pid, db_name)
        if socgrp_data:
            result.update({"soc_grp": socgrp_data})

    if "Workplace" in required_data:
        # Место работы
        workplace_data = await get_workplace(pid, db_name)
        if workplace_data:
            result.update({"work_place": workplace_data})

    if "Fiasaddress" in required_data:
        # Адрес ФИАС
        fiasaddr_data = await get_fias_addr(pid, db_name)
        if fiasaddr_data:
            result.update({"fias_addr": fiasaddr_data})

    cache_spr = cache.cache_get_dict(key=f'spr_{db_name}')
    result.update({'spr_versions': cache_spr or await get_spr_versions(db_name)})
    result.update({"family": await get_family_data(pid, db_name)})
    if not cache_spr:
        cache.cache_set_dict(key=f'spr_{db_name}', value=result['spr_versions'], cache_time=3600)

    return result
