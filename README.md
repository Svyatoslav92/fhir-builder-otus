# Fhir-builder
###  Сервис - строитель объектов FHIR из БД "ПК Здравоохранение"
### Разработчик: Мамзин Святослав msn@pkzdrav.ru

[![fhir](http://hl7.org/fhir/assets/images/fhir-logo-www.png)](http://hl7.org/fhir)

[![pipeline status](https://gitlab.com/Svyatoslav92/fhir-builder-otus/badges/main/pipeline.svg)](https://gitlab.com/Svyatoslav92/fhir-builder-otus/-/commits/main)

FHIR (англ. Fast Healthcare Interoperability Resources — ресурсы быстрого взаимодействия в сфере здравоохранения) — стандарт обмена медицинской информацией. Стандарт описывает форматы медицинских данных и обмен этими данными через REST API. FHIR является торговой маркой некоммерческой организации HL7 (Health Level Seven International) — стандарт обмена, управления и интеграции электронной медицинской информации.

Формат FHIR используется в Комтек как основной стандарт взаимодействия при обмене данными между различными сервисами и основным продуктом компании МИС “ПК Здравоохранение”. 
Одними из основных объектов в инфообмене являются данные пациента и медработника, и для единой точки формирования FHIR этих объектов было принято решение по разработке этого сервиса.


### Цели проекта:

- Разработка сервиса построения FHIR объектов пациента и медработника;
- Разработка модульных и интеграционных тестов сервиса; 
- Развертывание сервиса в Docker.

### Требования

- Сервис должен работать с несколькими БД;
- Сервис должен работать асинхронно;
- Сервис должен предоставлять API-метод для получения FHIR объекта из БД МИС по id с указанием аббревиатуры БД и id МО;
- Сервис должен принимать FHIR объект пациента и записывать в БД МИС;
- Тесты должны запускаться автоматически в Gitlab CI;
- Разворачиваться сервис должен через связку Jenkins+Ansible+Dokku.

Основой проекта стал фреймворк FastApi - это современная, высокопроизводительная веб-инфраструктура для создания API-интерфейсов с Python 3.6+ на основе стандартных подсказок типов Python.

#### Для реализации проекта также было использовано:

- [fhir.resources](https://github.com/nazrulworld/fhir.resources) - библиотека для построения FHIR моделей;
- [cx-oracle-async](https://pypi.org/project/cx-Oracle-async/) - низкоуровневая библиотека для асинхронной работы с БД Oracle;
- [Unicorn](https://www.uvicorn.org/) - для запуска FastApi приложения; 
- [Redis](https://redis.io/) - для кэширования;  
- [Poetry](https://python-poetry.org/) - для управления зависимостями сервиса;
- [Pytest](https://docs.pytest.org/en/7.1.x/contents.html) - для тестов;
- [flake8](https://pypi.org/project/flake8/) - для проверки стилистики кода;
- [Jenkins](https://www.jenkins.io/),[Ansible](https://www.ansible.com/), [Dokku](https://dokku.com/) - для автоматического развертывания на серверах.


### Алгоритм работы сервиса
[![shema](https://i.ibb.co/K6MKptj/fhir-img.png)](https://ibb.co/G7KH4Zv)

На данный момент реализован только один Get метод для получения FHIR объекта пациента с возможностью передачи в запрос списка элементов, из которых будет состоять объект, это если всех данных по объекту не требуется. Иначе если список не передается, то объект строится полностью.

```python
@app.get("/api/v1/{dbname}/lid/{lid}/Patient/{pid}", response_model=dict) >
async def get_bundle_patient(dbname: str, pid: int, lid: int, background_tasks: BackgroundTasks, elements: str = None):
    cache_patient = cache.cache_get_dict(f'{pid}_{dbname}_{elements}')
    elements_list = elements.split(",") if elements else None
    try:
        patient_data = cache_patient or await get_data_from_db_patient(pid, lid, dbname, required_data=elements_list)
        background_tasks.add_task(set_cache, f'{pid}_{dbname}_{elements}', patient_data)
        fhir = build_bundle_patient(patient_data)
    except (FHIRNotFoundError, FHIRBadRequest, FHIRServerError) as err:
        return err.response()

    return BundleModel(bundle=fhir).dict(exclude_none=True) 
```
В указанном методе обрабатывается запрос, и из этого запроса получаем аббревиатуру БД,
lid - id Мед.организации (МО) и id объекта. Аббревиатура БД нам нужна для того, чтобы было понятно к какой базе обращаться 
из указанных в конфиге сервиса. Идентификаторы МО и объекта необходимы для нахождения необходимых данных в БД.
В методе также используется background_tasks - это метод фреймворка FastApi для параллельного выполнения кода, в данном случае записи объекта в кэш.

### Асинхронная работа с БД Oracle
Изначально когда родилась идея писать сервис на FastApi, в планах было использование ORM,
например, такого как SQLAlchemy. Знакомства с этим фреймворком до этого не было. Единственное, что было 
известно, так это то, что он позволяет работать с БД асинхронно. На просторах интернета 
было много прекрасных примеров организации работы FastApi+SQLAlchemy. Но при подробном 
изучении SQLAlchemy выяснилось, что БД Oracle фреймворк умеет работать только синхронно.
Поэтому было решено использовать в работе библиотеку cx-oracle-async. 
Весь код для работы с БД находится в service/db_manager.

connector.py
```python
class DBConnector:

    def __init__(self, db_name):
        self.db_name = db_name
        self.con = None

    # creats new connection
    async def create_connection(self):
        return await cx_Oracle_async.create_pool(**DBS[self.db_name])
```
Данный класс инициализирует пулл соединений к БД переданной через инициализатор класса:
```python
class DBConnection:
   
    connections = {}

    @classmethod
    async def get_connection(cls, db_name, new=False):
        """Creates return new Singleton database connection"""
        if new or db_name not in cls.connections:
            logging.info(f"Новое подключение к {db_name}")
            cls.connections[db_name] = await DBConnector(db_name).create_connection()
        else:
            logging.info(f'Подключение к {db_name}')

        cls.connections[db_name].callTimeout = EXECUTE_TIMEOUT_SQL
        return cls.connections[db_name]
```
DBConnection - класс, который содержит классовый атрибут connections, в котором, в свою очередь, содержатся пуллы соединений к базам Oracle.
Получается такая реализация паттерна Singleton для использования одних и тех же сессий с БД.
Запросы к БД представляют собой сырой SQL который выполняется в методе execute_query. Все SQL-запросы можно найти в service/db_manager/sql_queries.py

Один из недостатков cx-oracle-async - это отсутствие метода для получения наименование колонок таблиц БД. К примеру, в 
синхронной версии cx-oracle у объекта курсора есть метод description, который возвращает наименование колонок. Это можно 
сопоставить с результатом метода fetchall() или fetchone(), который возвращает кортеж, и получить в результате удобный в использовании dict.
Для решения этой проблемы в функциях, которые и исполняют метод execute_query, явно прописаны списки полей из запроса. Это явно
не самое лучшее решение, т.к при изменении порядка и количества полей в SQL-запросе нужно учитывать и этот список в теле функции.
В дальнейшем это требует пересмотра. Пример функций со списком полей из service/db_manager/db_requests.py указан ниже:
```python
async def get_patient(pid, db_name):
    columns = ('family', 'old_family', 'name', 'father_name', 'sex', 'phone', 'shift_worker', 'snils', 'citizenship',
               'birth_date', 'birth_place', 'marital_status', 'education', 'email', 'hst0460', 'hst0460_name',
               'inv_group', 'nationality', 'id',)
    result = await DBConnection.execute_query(GetPassportByPid, params={'pid': pid}, db_name=db_name)
    return dict(zip(columns, result)) if result else None


async def get_hospital(lid, db_name):
    result = await DBConnection.execute_query(GetHospital, params={'lid': lid}, db_name=db_name)
    return {'mcod': result[0], 'fed_oid': result[1]} if result else None
```
### Fhir модели
Такие объекты как пациент и медработник представляют собой совокупность других объектов таких как адрес,
документ, удостоверяющий личность (ДУЛ), СНИЛС, полис и т.д. И для этих элементов в библиотеке FHIR.resources
предусмотренны готовые модели FHIR, от которых мы наследуемся при написании своих "кастомных" моделей.
Пример из service/fhir_models/fhir_models.py:
```python
class IdentityDocumentFHIR(Identifier):
    def __init__(
        self,
        series=None,
        number=None,
        start=None,
        end=None,
        issuer=None,
        doc_type_code=None,
        directory_version=None,
        doc_type_name=None,
    ):
        super().__init__()
        self._set_value(series, number)
        self.period = PeriodType(
            start=start,
            end=end,
        )
        self.assigner = ReferenceType(display=issuer)
        self._set_type(doc_type_code, doc_type_name, directory_version)
```

Стандарт FHIR также предусматривает формат response в случае не успешного запроса. 
Это могут быть некорректно переданные параметры в запрос, отсутствие данных в БД или ошибка в работе сервиса.
В таких случаях в response передаем сообщение в объекте OperationOutcome.

Аббревиатура БД не найдена в конфиге:
```json
{
  "resourceType": "OperationOutcome",
  "issue": [
    {
      "severity": "error",
      "code": "1",
      "details": {
        "text": "База данных определена неверно"
      },
      "diagnostics": "Доступные базы данных: ANCHOB, NVONKO, S_NVDS1, DBASE11, NFUGBDB"
    }
  ]
}
```
Пациент не найден в БД:
```json
{
  "resourceType": "OperationOutcome",
  "id": "11111",
  "issue": [
    {
      "severity": "error",
      "code": "2",
      "details": {
        "text": "Пациент 11111 не найден в БД NVONKO"
      }
    }
  ]
}
```
## Deploy
Для локального запуска сервиса в контейнере Doker необходимо:
1. В корне директории проекта запустить
```sh
docker build -t myimage .
```
2. Далее после того как построим образ, запускаем контейнер
```sh
docker run -d --name mycontainer -p 5000:5000 myimage
```
3. Подсмотреть лог контейнера мы сможем так
```sh
 docker logs mycontainer
```
Для разворачивания на продуктовых серверах добавлены и настроены скрипты Ansible /ansible_scripts.
В директории ansible_scripts/templates лежит шаблон конфига settings.j2, ansible при запуске "плейбука" deploy.yml
рендерит конфиги для каждого продуктового сервера на основе данных из inventory.yml. Для рендеринга ansible использует библиотеку jinja2.
Перед первым деплоем на сервер, необходимо подготовить dokku (создать приложение dokku, установить и настроить необходимый плагин, например Redis, настроить рабочий порт
для сервиса). Для этого существует отдельный "плейбук" ansible_scripts/create.yml, который при запуске проходит по 
серверам указанным в ansible_scripts/inventory.yml и все настраивает.

Подготавливаем Dokku на сервере, запускаем create.yml
```sh
 ansible-playbook create.yml
```
Разворачиваем сервис на сервере, запускаем deploy.yml
```sh
 ansible-playbook deploy.yml
```
В директории /ansible_scripts также можно увидеть passwords.yml файл с паролями для возможности 
выполнения различных команд ansible через sudo на серверах. Для того чтобы была возможность хранить 
этот файл в директории, у ansible существует метод шифрования:
```sh
ansible-vault encrypt passwords.yml
```

### Дальнейшее развитие
В сервисе FHIR-builder еще предстоит добавление метода для получения FHIR-объекта медработника и метода записи FHIR-объекта в БД МИС.
Также необходимо большее покрытие тестами, в особенности модульными.
## Итог

Разработан сервис FHIR-builder, который на данный момент предоставляет пока один метод
для получения Fhir объекта пациента. Написаны тесты для данного сервиса. Тесты настроены на автоматический запуск в Gitlab CI.  
В процессе разработки приобретен дополнительных опыт написания асинхронного кода (это мой второй проект, использующий асинхронщину python),
также впервые использовал poetry, до этого в основном обходился pip-ом. Настройка Gitlab CI для запуска тестов также была
впервые.  



