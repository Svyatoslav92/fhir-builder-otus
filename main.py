import json
import logging

import uvicorn
from fastapi import FastAPI, Request, status, BackgroundTasks
from fastapi.exceptions import RequestValidationError
from fhir.resources.bundle import Bundle
from pydantic import BaseModel

from conf.settings import DBS
from service.db_manager.connector import DBConnection
from service.db_manager.db_requests import get_data_from_db_patient
from service.fhir_models.fhir_exceptions import FHIRNotFoundError, FHIRBadRequest, FHIRServerError
from service.fhir_models.fhir_operation_outcome import build_operationoutcome
from service.fhir_models.fhir_patient_bundle import build_bundle_patient
from fastapi.responses import JSONResponse
from service.redis_store import RedisStore
from service.utils import set_cache


class BundleModel(BaseModel):
    bundle: Bundle


app = FastAPI()
cache = RedisStore()


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    list_issue = [{"code": 4,
                   "details": {"text": err['msg']},
                   "diagnostics": f"{err['type']}. location {err['loc']}"}
                  for err in exc.errors()]
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=json.loads(build_operationoutcome(list_issue).json()))


@app.on_event("startup")
async def startup():
    for db_name in DBS:
        try:
            await DBConnection.get_connection(db_name=db_name)
        except Exception as e:
            logging.warning(f"Не удалось подключится к БД {db_name}: {e}")
            continue


@app.on_event("shutdown")
def shutdown():
    DBConnection.close_connections()


@app.get("/api/v1/{dbname}/lid/{lid}/Patient/{pid}", response_model=dict)
async def get_bundle_patient(dbname: str, pid: int, lid: int, background_tasks: BackgroundTasks, elements: str = None):
    """
    метод возвращает Fhir обьект пациента
    :param dbname: аббревиатура БД МО
    :param pid: идентификатор пациента
    :param lid: идентификатор МО
    :param background_tasks:
    :param elements: строка из элементов объекта пациента переданных через запятую,
     если не передавать то строится полностью
    :return: BundleModel
    """
    cache_patient = cache.cache_get_dict(f'{pid}_{dbname}_{elements}')
    elements_list = elements.split(",") if elements else None
    try:
        patient_data = cache_patient or await get_data_from_db_patient(pid, lid, dbname, required_data=elements_list)
        background_tasks.add_task(set_cache, f'{pid}_{dbname}_{elements}', patient_data)
        fhir = build_bundle_patient(patient_data)
    except (FHIRNotFoundError, FHIRBadRequest, FHIRServerError) as err:
        return err.response()

    return BundleModel(bundle=fhir).dict(exclude_none=True)


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=5000)
