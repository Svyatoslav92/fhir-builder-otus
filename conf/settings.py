import os

DB_USER = {"user": os.environ.get("DB_USER", "exchange"),
           "password": os.environ.get("DB_PASS", "e8SzuHGn")}

# Количество сессий в пулле с БД
MIN = 5
MAX = 10

EXECUTE_TIMEOUT_SQL = 60000

REDIS_URL = os.getenv("REDIS_URL", 'redis://localhost:6379')
REDIS_SOCKET_TIMEOUT = 3

DBS = {
    # TEST DB,
    'S_NVDS1': {**DB_USER, 'host': '192.168.2.58', 'port': '1521', "service_name": "nvds1", "min": MIN, "max": MAX}
}

