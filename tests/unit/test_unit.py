from datetime import datetime as dt, timedelta
import unittest

from conf.settings import DBS
from service.fhir_models.fhir_exceptions import FHIRBadRequest
from service.utils import check_db, check_age, check_snils


def cases(cases_list):
    def deco(func):
        def wrapper(self):
            for case in cases_list:
                func(self, case)
        return wrapper
    return deco


class TestCheckDBFunc(unittest.TestCase):

    @cases([5, 'sdsa', '111', True, 'ab.com'])
    def test_check_db_incorrect_value(self, value):
        with self.assertRaises(FHIRBadRequest):
            check_db(value)

    @cases(list(DBS.keys()))
    def test_check_db_correct_value(self, value):
        self.assertTrue(check_db(value))


class TestCheckAgeFunc(unittest.TestCase):

    @cases([dt.strftime(dt.now(), "%Y-%m-%d"),
           dt.now()-timedelta(days=1),
           dt.now()-timedelta(days=59)])
    def test_check_age_return_true(self, value):
        self.assertTrue(check_age(value, 60))

    @cases([dt.strftime(dt.now()-timedelta(days=61), "%Y-%m-%d"),
           dt.now()-timedelta(days=60),
           dt.now()-timedelta(days=61)])
    def test_check_age_return_false(self, value):
        self.assertFalse(check_age(value, 60))


class TestCheckSnils(unittest.TestCase):

    @cases([None, '', '111111222', True, '21321321421412'])
    def test_check_snils_incorrect_value(self, value):
        self.assertFalse(check_snils(value))

    @cases(['052-806-098 51', '17738122085', '120-341-48095'])
    def test_check_snils_correct_value(self, value):
        self.assertTrue(check_snils(value))
