from fastapi.testclient import TestClient
from service.db_manager import db_requests

from tests.data.data_for_testes import override_patient, override_hospital, override_policy, override_smk, \
    override_docum, override_socgrp, override_workplace, override_spr_versions, override_family, override_fias_address

# подмена данных из БД на тестовые данные
db_requests.get_patient = override_patient
db_requests.get_hospital = override_hospital
db_requests.get_policy = override_policy
db_requests.get_docum = override_docum
db_requests.get_socgrp = override_socgrp
db_requests.get_smk = override_smk
db_requests.get_workplace = override_workplace
db_requests.get_fias_addr = override_fias_address
db_requests.get_spr_versions = override_spr_versions
db_requests.get_family_data = override_family

from main import app # NOQA

client = TestClient(app)


def test_override_patient_data():
    response = client.get("/api/v1/S_NVDS1/lid/117/Patient/100")
    assert response.status_code == 200
    assert response.json().get("bundle") is not None
    assert response.json().get("bundle")["entry"][0]["resource"]["id"] == '1229251'
    addr_fias = response.json().get("bundle")["entry"][0]["resource"]["address"]
    assert len(addr_fias) == 2
    assert addr_fias[0]["use"] == "home"
    assert addr_fias[1]["use"] == "billing"
    identifier = response.json().get("bundle")["entry"][0]["resource"]["identifier"]
    assert len(identifier) != 0
    maritalStatus = response.json().get("bundle")["entry"][0]["resource"]["maritalStatus"]
    assert len(maritalStatus) != 0
    name = response.json().get("bundle")["entry"][0]["resource"]["name"]
    assert len(name) != 0
    telecom = response.json().get("bundle")["entry"][0]["resource"]["telecom"]
    assert len(telecom) != 0
    assert len(response.json().get("bundle")["entry"]) == 3
    assert response.json().get("bundle")["type"] == "collection"
    assert response.json().get("bundle")["resourceType"] == "Bundle"


def test_invalid_request():
    response = client.get("/api/v1/S_NVDS1/lid/117/Patient/фывфы")
    assert response.status_code == 422
    assert response.json() == {
        "issue": [{"code": "4", "details": {"text": "value is not a valid integer"},
                   "diagnostics": "type_error.integer. location ('path', 'pid')", "severity": "error"}],
        "resourceType": "OperationOutcome"}

    response = client.get("/api/v1/S_NVDS1/lid/ффф/Patient/001")
    assert response.status_code == 422
    assert response.json() == {
        "issue": [{"code": "4", "details": {"text": "value is not a valid integer"},
                   "diagnostics": "type_error.integer. location ('path', 'lid')", "severity": "error"}],
        "resourceType": "OperationOutcome"}

    response = client.get("/api/v1/S_NVDS1/lid/ффф/Patient/001aa")
    assert response.status_code == 422
    assert response.json() == {"issue": [{"code": "4", "details": {"text": "value is not a valid integer"},
                                          "diagnostics": "type_error.integer. location ('path', 'pid')",
                                          "severity": "error"},
                                         {"code": "4", "details": {"text": "value is not a valid integer"},
                                          "diagnostics": "type_error.integer. location ('path', 'lid')",
                                          "severity": "error"}],
                               "resourceType": "OperationOutcome"}


def test_non_existent_databases():
    response = client.get("/api/v1/фыыф/lid/117/Patient/001")
    assert response.status_code == 400
    assert len(response.json()["issue"]) == 1
    assert response.json()["issue"][0]["code"] == '1'
    assert response.json()["issue"][0]["details"]["text"] == "База данных определена неверно"


def test_object_not_found():
    response = client.get("/api/v1/S_NVDS1/lid/117/Patient/1001")
    assert response.status_code == 404
    assert response.json() == {
        "id": "1001", "issue": [
            {
                "code": "2",
                "details": {
                    "text": "Пациент 1001 не найден в БД S_NVDS1"
                },
                "severity": "error"
            }
        ],
        "resourceType": "OperationOutcome"
    }
