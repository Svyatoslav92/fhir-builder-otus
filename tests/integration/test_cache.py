import json
import unittest
from time import sleep
from conf.settings import REDIS_URL
from service.redis_store import RedisStore
from tests.data.data_for_testes import fias_address, family, spr_versions, workplace, socgrp, docum, smk, policy, \
    hospital, patient
from tests.unit.test_unit import cases


def get_redis_status():
    store = RedisStore()
    if not REDIS_URL:
        return True
    try:
        store._cache_set('key', 'value')
        store._cache_get('key')
        return False
    except Exception:
        return True


REDIS_IS_NOT_RUN = get_redis_status()


class TestStore(unittest.TestCase):
    store = RedisStore()

    @unittest.skipIf(REDIS_IS_NOT_RUN, "Redis is not running")
    def test_cache_set(self):
        self.assertTrue(self.store._cache_set('key1', 'value1'))

    @unittest.skipIf(REDIS_IS_NOT_RUN, "Redis is not running")
    def test_cache_get(self):
        self.store._cache_set('key2', 'value2')
        value = self.store._cache_get('key2')
        self.assertEqual(value.decode('UTF-8'), 'value2')

    @unittest.skipIf(REDIS_IS_NOT_RUN, "Redis is not running")
    def test_cache_timeout(self):
        self.store._cache_set('key3', 'value3', cache_time=1)
        sleep(2)
        value = self.store._cache_get('key3')
        self.assertEqual(value, None)

    @unittest.skipIf(REDIS_IS_NOT_RUN, "Redis is not running")
    def test_set_dict(self):
        self.assertTrue(self.store.cache_set_dict('key4', {1: 1}))

    @unittest.skipIf(REDIS_IS_NOT_RUN, "Redis is not running")
    @cases([(1, fias_address), (2, family), (3, spr_versions),
            (4, workplace), (5, socgrp), (6, docum), (7, smk),
            (8, policy), (9, hospital), (10, patient)])
    def test_get_dict(self, value):
        self.store.cache_set_dict(f'key{value[0]}', value[1])
        cache = self.store.cache_get_dict(f'key{value[0]}')
        self.assertEqual(cache, json.loads(json.dumps(value[1], default=str)))
