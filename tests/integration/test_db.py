import unittest
import asyncio

from conf.settings import DBS
from service.db_manager.connector import DBConnection
from service.db_manager.sql_queries import GetHospital, GetPassportByPid, GetPolicyByPid, GetPDocumByPid, \
    GetFamilyByPid, GetSocGrpByPid, GetSMKByLid, GetWorkPlaceByPid, GetFiasAddress
from tests.unit.test_unit import cases


async def get_db_status():
    # если нет параметров подключения к тестовой базе, то return True
    # eсли нет соединения до тестовой БД, то return True
    if DBS.get('S_NVDS1') is None or not DBS.get('S_NVDS1').get("user") or not DBS.get('S_NVDS1').get("password"):
        return True
    con = DBConnection()
    try:
        await con.get_connection(db_name='S_NVDS1')
        return False
    except Exception:
        return True


STATUS_SKIP = asyncio.run(get_db_status())


class TestDB(unittest.TestCase):

    @unittest.skipIf(STATUS_SKIP, "")
    async def setUp(self):
        self.conn = DBConnection()
        await self.conn.get_connection(db_name='S_NVDS1')

    @unittest.skipIf(STATUS_SKIP, "")
    async def tearDown(self):
        await self.conn.close_connections()

    @unittest.skipIf(STATUS_SKIP, "")
    @cases([(GetHospital, {'lid': 117}),
            (GetPassportByPid, {'pid': 11113}),
            (GetPolicyByPid, {'pid': 11113}),
            (GetPDocumByPid, {'pid': 11113}),
            (GetFamilyByPid, {'pid': 11113}),
            (GetSocGrpByPid, {'pid': 11113}),
            (GetSMKByLid, {'lid': 117}),
            (GetWorkPlaceByPid, {'pid': 11113}),
            (GetFiasAddress, {'pid': 11113})
            ])
    async def test_execute_query(self, query):
        data = await self.conn.execute_query(query[0],
                                             params=query[1],
                                             db_name='S_NVDS1',
                                             many=True)
        self.assertIsNotNone(data)
