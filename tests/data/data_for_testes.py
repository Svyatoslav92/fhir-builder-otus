import datetime

patient = {'birth_date': datetime.datetime(1977, 2, 7, 0, 0),
           'birth_place': 'УКРАИНА, ОДЕССА',
           'citizenship': 643,
           'education': 1,
           'email': 'test@test.ru',
           'family': 'Тестов',
           'father_name': 'Тестович',
           'hst0460': None,
           'hst0460_name': None,
           'id': 1229251,
           'inv_group': None,
           'marital_status': None,
           'name': 'Тест',
           'nationality': 1,
           'old_family': 'Тестиков',
           'phone': '+79227616000',
           'sex': 1,
           'shift_worker': 0,
           'snils': '076-471-494 94'}

hospital = {'fed_oid': '1.2.643.5.1.13.13.12.2.86.8895', 'mcod': 14101}

policy = {'enp': '8157220842001214',
          'lid': 1009,
          'policy_begin': datetime.datetime(2017, 5, 3, 0, 0),
          'policy_end': None,
          'policy_number': '8157220842001214',
          'policy_series': None,
          'policy_type_name': 'Полис ОМС единого образца',
          'rid_hmao': 3}

smk = {'smo_code': '81008',
       'smo_id': 196,
       'smo_name': 'ХАНТЫ-МАНСИЙСКИЙ ФИЛИАЛ ООО "АЛЬФАСТРАХОВАНИЕ -ОМС"',
       'smo_okato': 71100,
       'smo_terr': 'Ханты-Мансийский автономный округ - Югра'}

docum = [{'date_v': datetime.datetime(2022, 2, 17, 0, 0),
          'kem': 'УМВД РОССИИ ПО ХМАО ЮГРЕ',
          'name': 'Паспорт гражданина Российской Федерации',
          'num': '0002111',
          'rid_fed': 1,
          'ser': '66 22'}]

socgrp = {'fed_id': 4, 'name': 'РАБОТАЮЩИЙ', 'social': 5}

workplace = {'company_name': 'ООО  СЕРВИСНЕФТЕГАЗ', 'position_name': 'МАШИНИСТ'}

spr_versions = {'1.2.643.5.1.13.13.11.1038': '11.1',
                '1.2.643.5.1.13.13.99.2.48': '4.1',
                'C51007': '2.1',
                'C51009': '4.1',
                'FRMO_DEPART': '2.1067',
                'FRMO_SUBDIV': '2.977',
                'FRMR_POST': '4.2',
                'FRMR_SPEC': '1.4',
                'HST0010': '1.0',
                'HST0065': '1.2',
                'HST0080': '1.1',
                'HST0460': '1.0',
                'O00015': '1.1',
                'STR337': '1.2',
                'STR470': '1.1',
                'fed_soc_status': '1.2',
                'fed_soc_status_emp': '1.2'}

family = [{'fias_address': [{'city': 'da7ef573-cdbe-458e-a790-b29ad3a3d140',
                             'district': None,
                             'flat': None,
                             'house': '0d7547d3-70d3-4830-a43d-6fb0ba490ee6',
                             'state': '6f2cbfd8-692a-4ee4-9b16-067210bde3fc',
                             'street': 'a99bf5cc-8292-4d47-98ff-caf093c42b9b',
                             'text': 'Респ Башкортостан, р-н Благовещенский, г '
                                     'Благовещенск, ул Седова, д.49',
                             'use': 'home'},
                            {'city': '45906532-143b-48c2-9af3-f480dc19c7bf',
                             'district': None,
                             'flat': '19',
                             'house': '12aa32bf-0db2-4b54-8b5e-1d9510d44b6e',
                             'state': 'd66e5325-3a25-4d29-ba86-4ca351d9704b',
                             'street': '298d4989-39e0-4edc-a262-b16c161e26e7',
                             'text': 'АО Ханты-Мансийский автономный округ - Югра, г '
                                     'Нефтеюганск, мкр. 16-й, д.42, кв.19',
                             'use': 'billing'}],
           'member': {'fname': 'АЛЛА',
                      'mname': 'Тестова',
                      'phone': '+79170420000',
                      'pid': 2307240,
                      'sname': 'Тестотестовая'}}]

fias_address = [{'city': '45906532-143b-48c2-9af3-f480dc19c7bf',
                 'district': None,
                 'flat': '64',
                 'house': '1ec2e1ad-521a-4cb9-b4f4-434954e0119c',
                 'state': 'd66e5325-3a25-4d29-ba86-4ca351d9704b',
                 'street': '29ff4f14-cc6f-49d2-a1c3-a62a9ec46b29',
                 'text': 'АО Ханты-Мансийский автономный округ - Югра, г Нефтеюганск, мкр. '
                         '12, д.5, кв.64',
                 'use': 'home'},
                {'city': '45906532-143b-48c2-9af3-f480dc19c7bf',
                 'district': None,
                 'flat': '64',
                 'house': '1ec2e1ad-521a-4cb9-b4f4-434954e0119c',
                 'state': 'd66e5325-3a25-4d29-ba86-4ca351d9704b',
                 'street': '29ff4f14-cc6f-49d2-a1c3-a62a9ec46b29',
                 'text': 'АО Ханты-Мансийский автономный округ - Югра, г Нефтеюганск, мкр. '
                         '12, д.5, кв.64',
                 'use': 'billing'}]


async def override_patient(pid, *ar, **kw) -> dict:
    return patient if pid == 100 else None


async def override_hospital(*ar, **kw) -> dict:
    return hospital


async def override_policy(*ar, **kw) -> dict:
    return policy


async def override_smk(*ar, **kw) -> dict:
    return smk


async def override_docum(*ar, **kw) -> list:
    return docum


async def override_socgrp(*ar, **kw) -> dict:
    return socgrp


async def override_workplace(*ar, **kw) -> dict:
    return workplace


async def override_fias_address(*ar, **kw) -> list:
    return fias_address


async def override_family(*ar, **kw) -> list:
    return family


async def override_spr_versions(*ar, **kw) -> dict:
    return spr_versions
